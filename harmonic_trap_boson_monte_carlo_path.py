import datetime
import random, math, numpy as np, sys, os
import matplotlib.pyplot as plt
import multiprocessing
from scipy import stats

def harmonic_ground_state(x):
    return math.exp(-x ** 2)/math.sqrt(math.pi)

def levy_harmonic_path_3d(k,beta):
    x0 = tuple([random.gauss(0.0, 1.0 / math.sqrt(2.0 *
                math.tanh(k * beta / 2.0))) for d in range(3)])
    x = [x0]
    for j in range(1, k):
        Upsilon_1 = 1.0 / math.tanh(beta) + 1.0 / \
                          math.tanh((k - j) * beta)

        try:
            Upsilon_2 = [x[j - 1][d] / math.sinh(beta) + x[0][d] /
                         math.sinh((k - j) * beta) for d in range(3)]
        except:
            Upsilon_2 = [x[j - 1][d] / math.sinh(beta) for d in range(3)]
        x_mean = [Upsilon_2[d] / Upsilon_1 for d in range(3)]
        sigma = 1.0 / math.sqrt(Upsilon_1)
        dummy = [random.gauss(x_mean[d], sigma) for d in range(3)]
        x.append(tuple(dummy))
    return x

def rho_harm_3d(x, xp,beta):
    Upsilon_1 = sum((x[d] + xp[d]) ** 2 / 4.0 *
                    math.tanh(beta / 2.0) for d in range(3))
    Upsilon_2 = sum((x[d] - xp[d]) ** 2 / 4.0 /
                    math.tanh(beta / 2.0) for d in range(3))
    return math.exp(- Upsilon_1 - Upsilon_2)
def start(N,T):
    print("Start at:")
    print(datetime.datetime.now())
    # N = 64
    T_star = T
    list_T = np.linspace(T+0.001,T-0.001,3)
    print(list_T)

    cycle_min = 10
    if T>0.5:
        nsteps = 10000000
    elif T > 0.2:
        nsteps = 1000000
    else:
        nsteps = 100000

    data_x, data_y, data_x_l, data_y_l = [], [], [], []
    location = "imgs/"+str(N)+"_"+str(nsteps)

    try:
        os.mkdir(location)

    except:
        pass
    final_prob_k = [0]*(N+1)
    for T_star in list_T:
        beta = 1.0 / (T_star * N ** (1.0 / 3.0))
        # Initial condition
        filename = location+'/data_boson_configuration_N%i_T%.1f.txt' % (N,T_star)
        positions = {}
        number_of_k = [0] * N
        if os.path.isfile(filename)and False:
            f = open(filename, 'r')
            for line in f:
                a = line.split()
                positions[tuple([float(a[0]), float(a[1]), float(a[2])])] = \
                       tuple([float(a[3]), float(a[4]), float(a[5])])
            f.close()
            if len(positions) != N:
                sys.exit('ERROR in the input file.')
            print ('starting from file', filename)
        else:
            for k in range(N):
                a = levy_harmonic_path_3d(1,beta)
                positions[a[0]] = a[0]
            print ('Starting from a new configuration',T_star)

        # Monte Carlo loop
        for step in range(nsteps):
            # move 1: resample one permutation cycle
            boson_a = random.choice(list(positions.keys()))
            perm_cycle = []
            while True:
                perm_cycle.append(boson_a)
                boson_b = positions.pop(boson_a)
                if boson_b == perm_cycle[0]:
                    break
                else:
                   boson_a = boson_b
            k = len(perm_cycle)

            data_x.append(boson_a[0])
            data_y.append(boson_a[1])
            final_prob_k[k] += 1
            if k > cycle_min:
                data_x_l.append(boson_a[0])
                data_y_l.append(boson_a[1])

            perm_cycle = levy_harmonic_path_3d(k,beta)
            positions[perm_cycle[-1]] = perm_cycle[0]
            for k in range(len(perm_cycle) - 1):
                positions[perm_cycle[k]] = perm_cycle[k + 1]

            # move 2: exchange
            a_1 = random.choice(list(positions.keys()))
            b_1 = positions.pop(a_1)
            a_2 = random.choice(list(positions.keys()))
            b_2 = positions.pop(a_2)
            weight_new = rho_harm_3d(a_1, b_2,beta) * rho_harm_3d(a_2, b_1,beta)
            weight_old = rho_harm_3d(a_1, b_1,beta) * rho_harm_3d(a_2, b_2,beta)
            if random.uniform(0.0, 1.0) < weight_new / weight_old:
                positions[a_1] = b_2
                positions[a_2] = b_1
            else:
                positions[a_1] = b_1
                positions[a_2] = b_2

        f = open(filename, 'w')
        for a in positions:
           b = positions[a]
           f.write(str(a[0]) + ' ' + str(a[1]) + ' ' + str(a[2]) + ' ' +
                   str(b[0]) + ' ' + str(b[1]) + ' ' + str(b[2]) + '\n')
        f.close()

        # Analyze cycles, do 3d plot
        import pylab, mpl_toolkits.mplot3d




        fig = pylab.figure()
        ax = mpl_toolkits.mplot3d.axes3d.Axes3D(fig)
        ax.set_aspect('equal')
        n_colors = 10
        list_colors = ["#a048f8",
"#938900",
"#4c8dff",
"#004d3b",
"#ff51bd",
"#77f3ff",
"#1e004f",
"#ffc2af",
"#410025",
"#cfc6ff"]#pylab.cm.rainbow(np.linspace(0, 1, n_colors))[::-1]
        dict_colors = {}
        i_color = 0
        positions_copy = positions.copy()
        while positions_copy:
            x, y, z = [], [], []
            starting_boson = list(positions_copy.keys())[0]
            boson_old = starting_boson
            while True:
                x.append(boson_old[0])
                y.append(boson_old[1])
                z.append(boson_old[2])
                boson_new = positions_copy.pop(boson_old)
                if boson_new == starting_boson: break
                else: boson_old = boson_new
            len_cycle = len(x)
            if len_cycle > 2:
                x.append(x[0])
                y.append(y[0])
                z.append(z[0])
            if len_cycle in dict_colors:
                color = dict_colors[len_cycle]
                ax.plot(x, y, z, '+-', c=color, lw=0.75)
            else:
                color = list_colors[i_color]
                i_color = (i_color + 1) % n_colors
                dict_colors[len_cycle] = color
                ax.plot(x, y, z, '+-', c=color, label='k=%i' % len_cycle, lw=0.75)
        pylab.title(str(N) + ' bosons at T* = ' + str(T_star))
        pylab.legend()
        ax.set_xlabel('$x$', fontsize=16)
        ax.set_ylabel('$y$', fontsize=16)
        ax.set_zlabel('$z$', fontsize=16)
        xmax = 6.0
        ax.set_xlim3d([-xmax, xmax])
        ax.set_ylim3d([-xmax, xmax])
        ax.set_zlim3d([-xmax, xmax])
        if T_star == T:
            pylab.savefig(location+'/plot_boson_configuration_N%i_T%.3f.svg' %(N,T_star))
        # pylab.show()
        pylab.close()

    #Plot the histograms
    list_x = [0.1 * a for a in range (-50, 51)]
    y = [harmonic_ground_state(a) for a in list_x]
    pylab.plot(list_x, y, '--k', linewidth=2.0, label='Ground state')
    try:
        pylab.hist(data_x, color='#0368ff', density=True, bins=120, alpha=0.7, label='All bosons')
    except:
        pass
    pylab.hist(data_x_l, color='#6eff03', density=True, bins=120, alpha=0.6, label='Bosons in longer cycle')
    pylab.xlim(-3.0, 3.0)
    pylab.xlabel('$x$',fontsize=14)
    pylab.ylabel('$\pi(x)$',fontsize=14)
    pylab.title('3-d non-interacting bosons $x$ distribution $N= %i$, $T= %.3f$' %(N,T))
    pylab.legend()
    pylab.savefig(location+'/position_distribution_N%i_T%.3f.svg' %(N,T))
    # pylab.show()
    pylab.close()

    test_x = data_x
    test_y = data_y
    #
    # with open('x.txt', 'w') as f:
    #     for item in test_x:
    #         f.write("%s\n" % item)
    # with open('y.txt', 'w') as f:
    #     for item in test_y:
    #         f.write("%s\n" % item)

    ks = []
    with open(location+"/probablity_K_"+str(N)+"_"+str(T)+".txt","w") as f:
        f.write("%s\n" %str(N))
        for i in range(1, N+1):
            final_prob_k[i] = final_prob_k[i] / (3.0*nsteps)
            f.write("%s\n" % str(final_prob_k[i]))
            ks.append(i)

    # print(final_prob_k)
    # plt.scatter(ks, final_prob_k[1:])
    # plt.ylim((0, 0.003))
    # # if T_star == T:
    # plt.show()
    # plt.close()

    xy = np.vstack([test_x, test_y])
    density = stats.gaussian_kde(xy)
    nbins = 120
    limx = 10
    limy = 10
    limz = 0.3
    # print(data_x)
    # print(data_y)
    # xi, yi = np.mgrid[min(test_x):max(test_x):nbins * 1j, min(test_y):max(test_y):nbins * 1j]
    xi, yi = np.mgrid[-1*limx:limx:nbins * 1j, -1*limy:limy:nbins * 1j]
    di = density(np.vstack([xi.flatten(), yi.flatten()]))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(xi, yi, di.reshape(xi.shape), cmap=pylab.cm.viridis,
                    linewidth=0, antialiased=False)
    ax.set_zlabel('\N{GREEK SMALL LETTER PI}(x,y)')
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.title('The distribution of the $x$ and $y$ positions $N= %i$, $T= %.3f$' %(N,T))
    # plt.colorbar()
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.xlim(-1*limx, limx)
    plt.ylim(-1*limy, limy)
    ax.set_zlim3d(0,limz)
    plt.savefig(location + '/The distribution of the x and y positions_N%i_T%.3f.svg' % (N, T))
    ax.view_init(91, 90)
    plt.savefig(location + '/The distribution of the x and y positions_N%i_T%.3f_top_view.svg' % (N, T))

    # plt.show()
    plt.close()

    plt.hist2d(data_x, data_y, bins=120, normed=True)
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.title('The distribution of the $x$ and $y$ positions')
    plt.colorbar()
    plt.xlim(-1 * limx, limx)
    plt.ylim(-1 * limy, limy)
    plt.savefig(location + '/The distribution of the x and y positions_N%i_T%.3f_2D.svg' % (N, T))
    # plt.show()
    plt.close()
    print("END at")
    print(datetime.datetime.now())


if __name__ == '__main__':
    n = 50
    T = 0.1
    beta = 1.0 / (T * n ** (1.0 / 3.0))
    x = levy_harmonic_path_3d(n,beta)
    y = []
    dy = []
    ly = 0
    cy = 0
    plt.xlim(0,n)
    z = range(n)
    z1 = []
    bars = []
    for i in range(0,n,1):
        bars.append(([-5,5],[i*(beta/n),i*(beta/n)]))
    for i,j in zip(x,z):
        plt.clf()
        plt.xlim(-5, 5)
        plt.ylim(0, beta)

        if j == 0 or j==n-1:
            y.append(0)
        else:
            y.append(i[0])
        cy = i[0]
        dy.append(ly-cy)
        ly = cy
        z1.append(j*(beta/n))
        for k in bars:
            plt.plot(k[0],k[1],color='#BBBBBB')
        plt.plot(y[:], z1[:], 'r')
        plt.ylabel("$\\Delta\\tau$",fontsize=18)
        plt.xlabel("$x$",fontsize=18)
        plt.title("Lévy free path contribution",fontsize = 18)
        plt.plot([y[0],y[-1]],[z1[0],z1[-1]], 'g')
        plt.savefig('imgs/path_at_T%.3f_%i.svg' % ( T,j))

        # if j%20 == 0:
        plt.pause(0.05)

    plt.show()