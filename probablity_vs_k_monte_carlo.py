location = "imgs/"
N = 0
list = []
import math, pylab

def z(k, beta):
    return 1.0 / (1.0 - math.exp(- k * beta)) ** 3 #partition function of a single particle in a harmonic trap

def canonic_recursion(N, beta): #Landsberg recursion relations for the partition function of N bosons
    Z = [1.0] #Z_0 = 1
    for M in range(1, N + 1):
        Z.append(sum(Z[k] * z(M - k, beta) \
                     for k in range(M)) / M)
    return Z #list of partition functions for boson numbers up to N

def start(T_star = 0.5):
    N = 512#number of bosons
    # T_star = 0.5 #temperature
    beta = 1.0 / N ** (1.0 / 3.0) / T_star
    Z = canonic_recursion(N, beta) #partition function
    pi_k = [(z(k, beta) * Z[N - k] / Z[-1]) / float(N) for k in range(1, N + 1)] #probability of a cycle of length k
    # graphics output
    pylab.plot(range(1, N + 1), pi_k, lw=2.5)
    pylab.ylim(0.0, 0.003)
    pylab.xlabel('cycle length $k$', fontsize=16)
    pylab.ylabel('cycle probability $\pi_k$', fontsize=16)
    pylab.title('Cycle length distribution ($N=%i$)' % (N), fontsize=16)
    pylab.pause(1)
    # pylab.savefig('plot-prob_cycle_length.png')


if __name__ == '__main__':
    legend = []
    files = []

    for i in  [0.1,0.6,0.8]:
        files.append(location + "probablity_K_" + str(512) + "_" + str(i) + ".txt")
        start(i)
        legend.append("T = %.2f K"%i)
    pylab.legend(legend)
    # location = "imgs/" + str("600") + "_" + str(1000000)
    pylab.savefig(location + 'Probablity_vs_K.svg')

    for file in files:
        with open(file, 'r') as f:
            lines = f.readlines()
            N = int(lines[0].replace("\n", ""))
            for line in lines[1:]:
                for i in line.replace('[','').replace(']','').split(','):
                    list.append(float(i))

        x = range(0, N + 1)
        import matplotlib.pyplot as plt
        print(len(list))
        pylab.scatter(x, list,s=5)
        list.clear()
        pylab.ylim(0, 0.003)
    pylab.show()

    pass
