import random, math, numpy as np, sys, os
import matplotlib.pyplot as plt

from scipy import stats


N = 512
# T_star = 0.8
# list_T = np.linspace(0.6,0.601,2)
# print(list_T)

cycle_min = 10
nsteps = 1000000
location = "test_imgs/"+str(N)+"_"+str(nsteps)
def z(k, beta):
    return 1.0 / (1.0 - math.exp(- k * beta)) ** 3 #partition function of a single particle in a harmonic trap


try:
    os.mkdir(location)
except:
    pass

def harmonic_ground_state(x):
    return math.exp(-x ** 2)/math.sqrt(math.pi)
    # return math.exp(-x ** 2 / 2.0) / math.pi ** 0.25

def levy_harmonic_path_3d(k,beta):
    x0 = tuple([random.gauss(0.0, 1.0 / math.sqrt(2.0 *
                math.tanh(k * beta / 2.0))) for d in range(3)])
    x = [x0]
    for j in range(1, k):
        Upsilon_1 = 1.0 / math.tanh(beta) + 1.0 / \
                          math.tanh((k - j) * beta)
        try:
            Upsilon_2 = [x[j - 1][d] / math.sinh(beta) + x[0][d] /
                         math.sinh((k - j) * beta) for d in range(3)]
        except:
            Upsilon_2 = [x[j - 1][d] / math.sinh(beta) for d in range(3)]
        x_mean = [Upsilon_2[d] / Upsilon_1 for d in range(3)]
        sigma = 1.0 / math.sqrt(Upsilon_1)
        dummy = [random.gauss(x_mean[d], sigma) for d in range(3)]
        x.append(tuple(dummy))
    return x

def rho_harm_3d(x, xp,beta):
    Upsilon_1 = sum((x[d] + xp[d]) ** 2 / 4.0 *
                    math.tanh(beta / 2.0) for d in range(3))
    Upsilon_2 = sum((x[d] - xp[d]) ** 2 / 4.0 /
                    math.tanh(beta / 2.0) for d in range(3))
    return math.exp(- Upsilon_1 - Upsilon_2)

data_x, data_y, data_x_l, data_y_l = [], [], [], []


def canonic_recursion(N, beta): #Landsberg recursion relations for the partition function of N bosons
    Z = [1.0] #Z_0 = 1
    for M in range(1, N + 1):
        Z.append(sum(Z[k] * z(M - k, beta) \
                     for k in range(M)) / M)
    return Z #list of partition functions for boson numbers up to N

def calculate_Z(ks, T_star,N):
    Z = [1.0]  # Z_0 = 1
    beta = 1.0 / (T_star * N ** (1.0 / 3.0))
    for M in ks:
        Z.append(sum(Z[k] * z(M - k, beta) \
                     for k in range(M)) / M)
    return Z

N0_N = []
T_N0_N = []
def start(N,T):
    list_T = np.linspace(T, T+0.01, 2)
    for T_star in list_T:
        beta = 1.0 / (T_star * N ** (1.0 / 3.0))
        # Initial condition
        filename = location+'/data_boson_configuration_N%i_T%.1f.txt' % (N,T_star)
        positions = {}
        if os.path.isfile(filename):
            f = open(filename, 'r')
            for line in f:
                a = line.split()
                positions[tuple([float(a[0]), float(a[1]), float(a[2])])] = \
                       tuple([float(a[3]), float(a[4]), float(a[5])])
            f.close()
            if len(positions) != N:
                sys.exit('ERROR in the input file.')
            print ('starting from file', filename)
        else:
            for k in range(N):
                a = levy_harmonic_path_3d(1,beta)
                positions[a[0]] = a[0]
            print ('Starting from a new configuration')

        # Monte Carlo loop
        for step in range(nsteps):
            # move 1: resample one permutation cycle
            boson_a = random.choice(list(positions.keys()))
            perm_cycle = []
            while True:
                perm_cycle.append(boson_a)
                boson_b = positions.pop(boson_a)
                if boson_b == perm_cycle[0]:
                    break
                else:
                   boson_a = boson_b
            k = len(perm_cycle)
            data_x.append(boson_a[0])
            data_y.append(boson_a[1])

            if k > cycle_min:
                data_x_l.append(boson_a[0])
                data_y_l.append(boson_a[1])
            perm_cycle = levy_harmonic_path_3d(k,beta)
            positions[perm_cycle[-1]] = perm_cycle[0]
            for k in range(len(perm_cycle) - 1):
                positions[perm_cycle[k]] = perm_cycle[k + 1]

            # move 2: exchange
            a_1 = random.choice(list(positions.keys()))
            b_1 = positions.pop(a_1)
            a_2 = random.choice(list(positions.keys()))
            b_2 = positions.pop(a_2)
            weight_new = rho_harm_3d(a_1, b_2,beta) * rho_harm_3d(a_2, b_1,beta)
            weight_old = rho_harm_3d(a_1, b_1,beta) * rho_harm_3d(a_2, b_2,beta)
            #Metropolis
            if random.uniform(0.0, 1.0) < weight_new / weight_old:
                positions[a_1] = b_2
                positions[a_2] = b_1
            else:
                positions[a_1] = b_1
                positions[a_2] = b_2

        f = open(filename, 'w+')
        for a in positions:
           b = positions[a]
           f.write(str(a[0]) + ' ' + str(a[1]) + ' ' + str(a[2]) + ' ' +
                   str(b[0]) + ' ' + str(b[1]) + ' ' + str(b[2]) + '\n')
        f.close()

        # Analyze cycles, do 3d plot
        import pylab, mpl_toolkits.mplot3d

        fig = pylab.figure()
        ax = mpl_toolkits.mplot3d.axes3d.Axes3D(fig)
        # ax.set_aspect('equal')
        n_colors = 10
        list_colors = pylab.cm.rainbow(np.linspace(0, 1, n_colors))[::-1]
        dict_colors = {}
        i_color = 0
        positions_copy = positions.copy()
        ks = []
        while positions_copy:
            x, y, z = [], [], []
            starting_boson = list(positions_copy.keys())[0]
            boson_old = starting_boson
            while True:
                x.append(boson_old[0])
                y.append(boson_old[1])
                z.append(boson_old[2])
                boson_new = positions_copy.pop(boson_old)
                if boson_new == starting_boson: break
                else: boson_old = boson_new
            len_cycle = len(x)
            ks.append(len_cycle)
            if len_cycle > 2:
                x.append(x[0])
                y.append(y[0])
                z.append(z[0])
            if len_cycle in dict_colors:
                color = dict_colors[len_cycle]
                ax.plot(x, y, z, '+-', c=color, lw=0.75)
            else:
                color = list_colors[i_color]
                i_color = (i_color + 1) % n_colors
                dict_colors[len_cycle] = color
                ax.plot(x, y, z, '+-', c=color, label='k=%i' % len_cycle, lw=0.75)
        pylab.title(str(N) + ' bosons at T* = ' + str(T_star))
        pylab.legend()
        ax.set_xlabel('$x$', fontsize=16)
        ax.set_ylabel('$y$', fontsize=16)
        ax.set_zlabel('$z$', fontsize=16)
        xmax = 6.0
        ax.set_xlim3d([-xmax, xmax])
        ax.set_ylim3d([-xmax, xmax])
        ax.set_zlim3d([-xmax, xmax])
        # pylab.savefig(location+'/plot_boson_configuration_N%i_T%.1f.svg' %(N,T_star))
        # pylab.show()
        pylab.close()
        Z = calculate_Z(ks,T_star,N)
        N0 = sum(Z[:-1]) / Z[-1]
        N0_N.append(N0/N)
        T_N0_N.append(T_star)


    #Plot the histograms
    list_x = [0.1 * a for a in range (-50, 51)]
    y = [harmonic_ground_state(a) for a in list_x]
    pylab.plot(list_x, y,'--k', linewidth=2.0, label='Ground state')
    pylab.hist(data_x,color='#0368ff', density=True, bins=120, alpha = 0.5, label='All bosons')
    pylab.hist(data_x_l,color='#6eff03', density=True, bins=120, alpha = 0.5, label='Bosons in longer cycle')
    pylab.xlim(-3.0, 3.0)
    pylab.xlabel('$x$',fontsize=14)
    pylab.ylabel('$\pi(x)$',fontsize=14)
    pylab.title('3-d non-interacting bosons $x$ distribution $N= %i$, $T= %.1f$' %(N,T_star))
    pylab.legend()
    pylab.savefig(location+'/position_distribution_N%i_T%.1f.svg' %(N,T_star))
    # pylab.show()
    pylab.close()
    with open(location+'/position_distribution_N%i_T%.1f.txt' %(N,T_star), 'w') as f:
        f.write("%s\n" % data_x)
        f.write("%s\n" % data_y)
        f.write("%s\n" % data_x_l)
        f.write("%s\n" % data_y_l)

    test_x = data_x_l
    test_y = data_y_l
    #
    # with open('x.txt', 'w') as f:
    #     for item in test_x:
    #         f.write("%s\n" % item)
    # with open('y.txt', 'w') as f:
    #     for item in test_y:
    #         f.write("%s\n" % item)

    # xy = np.vstack([test_x, test_y])
    # density = stats.gaussian_kde(xy)
    # nbins = 20
    # print(data_x_l)
    # print(data_y_l)
    # xi, yi = np.mgrid[min(test_x):max(test_x):nbins * 1j, min(test_y):max(test_y):nbins * 1j]
    # di = density(np.vstack([xi.flatten(), yi.flatten()]))
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_surface(xi, yi, di.reshape(xi.shape), cmap=pylab.cm.viridis,
    #                 linewidth=0, antialiased=False)
    # plt.xlabel('$x$')
    # plt.ylabel('$y$')
    # plt.title('The distribution of the $x$ and $y$ positions')
    # # plt.colorbar()
    # plt.xlim(-3.0, 3.0)
    # plt.ylim(-3.0, 3.0)
    # # plt.savefig(location+'/The distribution of the x and y positions_N%i_T%.1f.svg'%(N,T_star))
    # # plt.show()

    # n0_n.append(len(data_x_l) / len(data_x))
    return len(data_x_l) / len(data_x)

if __name__ == '__main__':
    try:
        os.mkdir(location)
    except:
        pass
    nnn = []
    # list_T = np.linspace(0.06, .1, 5)

    # list_T = np.concatenate((list_T1,list_T2))
    # print(list_T)
    Ns = [512]
    ts = []
    for number in Ns:
        n0_n = []
        t = []
        list_T = np.linspace(0.1, number ** (1 / 3.0), 10)
        for i in list_T:
            n0_n.append(start(number,T=i))
            t.append(i/number**(1/3.0))
        nnn.append(n0_n)
        ts.append(t)
        data_x, data_y, data_x_l, data_y_l = [], [], [], []


    head = "T,"
    for nns in Ns:
        head+=(str(nns)+",")
    head+="\n"
    line = ""
    with open('n0_n_vs_T.txt', 'w') as f:
        f.write(head)
        for i in range(len(list_T)):
            line = str(list_T[i])+","
            for j in range(len(Ns)):
                line+=str(nnn[j][i])+","
            line+="\n"
            f.write(line)

    for n,t in zip(nnn,ts):
        plt.plot(t,n)
    plt.savefig(location + '/data.svg')

    plt.show()